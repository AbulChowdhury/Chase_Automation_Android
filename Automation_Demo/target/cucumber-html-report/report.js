$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("FeatureFiles/Demo.feature");
formatter.feature({
  "line": 1,
  "name": "Demo",
  "description": "",
  "id": "demo",
  "keyword": "Feature"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "User is on alternate logon screen",
  "keyword": "When "
});
formatter.step({
  "line": 4,
  "name": "I should see the landing page",
  "keyword": "Then "
});
formatter.match({
  "location": "Demo_StepsDefinations.user_is_on_alternate_logon_screen()"
});
formatter.result({
  "duration": 39224779330,
  "status": "passed"
});
formatter.match({
  "location": "Demo_StepsDefinations.i_should_see_the_landing_page()"
});
formatter.result({
  "duration": 89965319,
  "error_message": "org.dom4j.InvalidXPathException: Invalid XPath expression: // Location path cannot end with //\r\n\tat org.dom4j.xpath.DefaultXPath.parse(DefaultXPath.java:360)\r\n\tat org.dom4j.xpath.DefaultXPath.\u003cinit\u003e(DefaultXPath.java:59)\r\n\tat org.dom4j.DocumentFactory.createXPath(DocumentFactory.java:230)\r\n\tat org.dom4j.tree.AbstractNode.createXPath(AbstractNode.java:207)\r\n\tat org.dom4j.tree.AbstractNode.selectSingleNode(AbstractNode.java:183)\r\n\tat com.Automation_Demo.utilities.LocatorReader.getLocator(LocatorReader.java:31)\r\n\tat com.Automation_Demo.helperFiles.DemoHelper.verifyElementIsAvailable(DemoHelper.java:23)\r\n\tat com.Automation_Demo.StepsDefinations.Demo_StepsDefinations.i_should_see_the_landing_page(Demo_StepsDefinations.java:29)\r\n\tat ✽.Then I should see the landing page(FeatureFiles/Demo.feature:4)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 6,
  "name": "Verify Basic Info screen is displayed when user taps on Enroll",
  "description": "",
  "id": "demo;verify-basic-info-screen-is-displayed-when-user-taps-on-enroll",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "User taps Enroll",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Basic Info screen is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I quit the application",
  "keyword": "Then "
});
formatter.match({
  "location": "Demo_StepsDefinations.user_taps_Enroll()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Demo_StepsDefinations.basic_Info_screen_is_displayed()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Demo_StepsDefinations.i_quit_the_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.background({
  "line": 2,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 3,
  "name": "User is on alternate logon screen",
  "keyword": "When "
});
formatter.step({
  "line": 4,
  "name": "I should see the landing page",
  "keyword": "Then "
});
formatter.match({
  "location": "Demo_StepsDefinations.user_is_on_alternate_logon_screen()"
});
formatter.result({
  "duration": 36659399410,
  "status": "passed"
});
formatter.match({
  "location": "Demo_StepsDefinations.i_should_see_the_landing_page()"
});
formatter.result({
  "duration": 433777,
  "error_message": "org.dom4j.InvalidXPathException: Invalid XPath expression: // Location path cannot end with //\r\n\tat org.dom4j.xpath.DefaultXPath.parse(DefaultXPath.java:360)\r\n\tat org.dom4j.xpath.DefaultXPath.\u003cinit\u003e(DefaultXPath.java:59)\r\n\tat org.dom4j.DocumentFactory.createXPath(DocumentFactory.java:230)\r\n\tat org.dom4j.tree.AbstractNode.createXPath(AbstractNode.java:207)\r\n\tat org.dom4j.tree.AbstractNode.selectSingleNode(AbstractNode.java:183)\r\n\tat com.Automation_Demo.utilities.LocatorReader.getLocator(LocatorReader.java:31)\r\n\tat com.Automation_Demo.helperFiles.DemoHelper.verifyElementIsAvailable(DemoHelper.java:23)\r\n\tat com.Automation_Demo.StepsDefinations.Demo_StepsDefinations.i_should_see_the_landing_page(Demo_StepsDefinations.java:29)\r\n\tat ✽.Then I should see the landing page(FeatureFiles/Demo.feature:4)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 11,
  "name": "Verify Basic Info screen is displayed when user taps on Log on",
  "description": "",
  "id": "demo;verify-basic-info-screen-is-displayed-when-user-taps-on-log-on",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "User taps Log On",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "BAU Logon screen is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I quit the application",
  "keyword": "Then "
});
formatter.match({
  "location": "Demo_StepsDefinations.user_taps_Log_On()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Demo_StepsDefinations.bAU_Logon_screen_is_displayed()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Demo_StepsDefinations.i_quit_the_application()"
});
formatter.result({
  "status": "skipped"
});
});