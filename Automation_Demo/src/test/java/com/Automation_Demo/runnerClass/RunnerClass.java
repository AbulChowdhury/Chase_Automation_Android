package com.Automation_Demo.runnerClass;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
					features = {"classpath:FeatureFiles"},
					glue = {"com.Automation_Demo.StepsDefinations","com.Automation_Demo.utilities"},
					plugin = {"pretty","html:target/cucumber-html-report"}//,
					//format = {"html:target/cucumber-html-report/cucumber-pretty"},
					//tags = {"@Demo"}
		)

public class RunnerClass {

}
