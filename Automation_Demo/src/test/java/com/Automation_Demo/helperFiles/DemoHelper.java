package com.Automation_Demo.helperFiles;

import org.junit.Assert;

import com.Automation_Demo.utilities.DriverHelper;
import com.Automation_Demo.utilities.LocatorReader;

import io.appium.java_client.AppiumDriver;

public class DemoHelper extends DriverHelper
{
	private LocatorReader locatorReader;
	
	@SuppressWarnings("rawtypes")
	public DemoHelper(AppiumDriver driver)
	{
		super(driver);		
		locatorReader = new LocatorReader("Demo.xml");
	}
	
	public void verifyElementIsAvailable(String locator)
	{
		locator = locatorReader.getLocator(locator);
		Assert.assertTrue(isElementPresent(locator));
	}
	
	public void tapOnElement(String locator)
	{
		locator = locatorReader.getLocator(locator);
		clickOn(locator);
	}
}
