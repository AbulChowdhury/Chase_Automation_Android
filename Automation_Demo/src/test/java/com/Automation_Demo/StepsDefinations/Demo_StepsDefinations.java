package com.Automation_Demo.StepsDefinations;

import org.openqa.selenium.WebDriver;

import com.Automation_Demo.helperFiles.DemoHelper;
import com.Automation_Demo.utilities.DriverTestCase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Demo_StepsDefinations
{
	DriverTestCase driverTestCase;
	static DemoHelper demoHelper;
	static WebDriver driver;


	@Given("^User is on alternate logon screen$")
	public void user_is_on_alternate_logon_screen() throws Throwable
	{
		driverTestCase = new DriverTestCase();
		demoHelper = new DemoHelper(driverTestCase.getDriver());
	}

	@Then("^I should see the landing page$")
	public void i_should_see_the_landing_page() throws Throwable
	{
		demoHelper.verifyElementIsAvailable("");
	}

	@When("^User taps Enroll$")
	public void user_taps_Enroll() throws Throwable
	{
		demoHelper.tapOnElement("");
	}

	@Then("^Basic Info screen is displayed$")
	public void basic_Info_screen_is_displayed() throws Throwable
	{
		demoHelper.verifyElementIsAvailable("");	
	}

	@When("^User taps Log On$")
	public void user_taps_Log_On() throws Throwable
	{
		demoHelper.tapOnElement("");
	}

	@Then("^BAU Logon screen is displayed$")
	public void bAU_Logon_screen_is_displayed() throws Throwable
	{
		demoHelper.verifyElementIsAvailable("");
	}
	
	@Then("^I quit the application$")
	public void i_quit_the_application() throws Throwable
	{
		driverTestCase.quitApplication();
	}
}
