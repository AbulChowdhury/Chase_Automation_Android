package com.Automation_Demo.utilities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class DriverHelper
{
	@SuppressWarnings("rawtypes")
	protected AppiumDriver driver;

	@SuppressWarnings("rawtypes")
	public DriverHelper(AppiumDriver appiumDriver)
	{
		driver = appiumDriver;	

	}

	@SuppressWarnings("rawtypes")
	public AppiumDriver getDriver()
	{
		return driver;
	}

	public void WaitForElementPresent(String locator, int timeoutSeconds)
	{

		for (int i = 0; i < timeoutSeconds; i++) 
		{
			if (isElementPresent(locator)) 
			{
				break;
			}

			try 
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}

	public Boolean isElementPresent(String locator) 
	{
		Boolean result = false;
		try 
		{
			getDriver().findElement(By.xpath(locator));
			result = true;
		}
		catch (Exception ex) 
		{

		}

		return result;
	}

	public void clickOn(String locator)
	{		
		this.WaitForElementPresent(locator, 10);
		Assert.assertTrue(isElementPresent(locator));
		WebElement el = getDriver().findElement(By.xpath(locator));			
		el.click();
	}
	
	public void hideKeypad()
	{
		try
		{
		Thread.sleep(2000);
		}
		catch (Exception e) 
		{

		}
		driver.navigate().back();
	}
}
