package com.Automation_Demo.utilities;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;


public class LocatorReader 
{
	private Document doc;

	public LocatorReader(String xmlName)
	{
		SAXReader reader = new SAXReader();
		PropertyReader prop = new PropertyReader();
		try
		{
			String localPath = prop.getFilePath() +File.separator+"src"+File.separator+"test"+File.separator+"java"+File.separator+"com"+File.separator+"Automation_Demo"+File.separator+"Locators"+File.separator;
			doc = reader.read(localPath + xmlName);
		}
		catch (DocumentException e) 
		{
			e.printStackTrace();
		}
	}

	public String getLocator(String locator) 
	{
		return doc.selectSingleNode("//" + locator.replace('.', '/')).getText();
	}
}