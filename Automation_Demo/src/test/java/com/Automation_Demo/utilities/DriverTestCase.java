package com.Automation_Demo.utilities;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;

public class DriverTestCase 
{

	@SuppressWarnings("rawtypes")
	public AppiumDriver driver;
	PropertyReader prop;

	protected String userName = "";
	protected String password = "";

	protected String name = "";

	@SuppressWarnings("rawtypes")
	public DriverTestCase() throws Exception
	{
		DesiredCapabilities capabilities = new DesiredCapabilities();
		prop = new PropertyReader();
		capabilities.setCapability("appium-version", prop.readApplicationFile("AppiumVersion"));
		capabilities.setCapability("platformName", prop.readApplicationFile("PlateForm"));
		capabilities.setCapability("platformVersion", prop.readApplicationFile("platformVersion"));
		capabilities.setCapability("deviceName", prop.readApplicationFile("DeviceName"));
		//capabilities.setCapability("noReset", false);
		//capabilities.setCapability("fullReset", true);
		capabilities.setCapability("app", prop.getFilePath()+File.separator+"ApkFile"+File.separator+"Chase.apk");
		capabilities.setCapability("appPackage", "com.chase.sig.android");
		capabilities.setCapability("app-wait-activity", "com.chase.sig.android.activity.HomeActivity");
		driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Thread.sleep(10000);
		System.out.println(driver.getPageSource());
		//driver.findElement(By.xpath("//android.widget.EditText[@text='User ID']")).sendKeys("demo");
		Thread.sleep(10000);
		driver.quit();
	}
	
	public void quitApplication()
	{
		driver.quit();
	}

	@SuppressWarnings("rawtypes")
	public AppiumDriver getDriver()
	{
		return driver;
	}
}