Feature: Demo
	Background:
		When User is on alternate logon screen
		Then I should see the landing page

Scenario: Verify Basic Info screen is displayed when user taps on Enroll
      When User taps Enroll
      Then Basic Info screen is displayed
      Then I quit the application
      
Scenario: Verify Basic Info screen is displayed when user taps on Log on
      When User taps Log On
      Then BAU Logon screen is displayed
      Then I quit the application